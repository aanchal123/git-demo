#include<stdio.h>
#include<stdlib.h>
typedef struct node{	
	int no;
	struct node *left, *right;
}node;
typedef node *tree;
void init(tree *t) {
	*t = NULL;
}

void insert(tree *t, int no) {
	int direction;
	node *tmp, *p, *q;
	tmp = (node*)malloc(sizeof(node));
	tmp->no = no;
	tmp->left = tmp->right;
	if(*t == NULL) {
		*t = tmp;
		return;
	}
	p = *t;
	while(p) {
		q = p;
		if(p->no > no) {
			p = p->left;
			direction = 1;
		}
		else {
			p = p->right;
			direction = 0;
		}
	}
	if(direction == 0) {
		q->right = tmp;
	}
	else {
		q->left = tmp;
	}
}
void check(tree t) {
	if(t == NULL) {
		return;
	}
	node *p;
	p = t;
	while(p) {
		if(p->left->no > p->no) {
			
			p = p->left;
		}
		else if(p->right->no < p->no){
			p = p->right;
		}
		else {
			printf("not a BST");
			return;
		}
	}
}
void traversal(tree t) {
	if(t == NULL) {
		return;
	}
	printf("%d", t->no);
	traversal(t->left);
	traversal(t->right);
}
int main(){
	tree t;
	init(&t);
	insert(&t, 4);
	insert(&t, 5);
	insert(&t, 7);
	insert(&t, 2);
	insert(&t, 8);
	traversal(t);
	check(t);
}